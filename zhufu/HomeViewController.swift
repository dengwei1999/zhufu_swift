//
//  HomeViewController.swift
//  zhufu
//
//  Created by dengwei on 15/11/23.
//  Copyright © 2015年 dengwei. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController,UIScrollViewDelegate,UICollectionViewDataSource,UICollectionViewDelegate {
    
    var collectionView1 : UICollectionView?
    var image_data = ["logo1","logo2","logo3","logo4","logo5","logo6","logo7","logo8","logo9","logo10","logo11","logo12","logo13","logo14","logo15","logo16","logo17","logo18"]
     var day_data = ["元旦","春节","情人节","元宵节","妇女节","愚人节","劳动节","母亲节","儿童节","端午节","父亲节","七夕节","教师节","中秋节","国庆节","感恩节","光棍节","圣诞节"]
    
    @IBOutlet var scrollview1: UIScrollView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "主页"
        self.scrollview1.backgroundColor = UIColor.whiteColor()
        self.scrollview1.delegate = self
        self.scrollview1.contentSize.width = self.view.frame.size.width
        self.scrollview1.contentSize.height = 600
        //self.collectionView1?.scrollEnabled = false
        let in_width = UIScreen.mainScreen().bounds.size.width//获取屏幕宽
        let in_height  = ((in_width - 60)/3)  + 15
        let in_height2  = (((in_width - 60)/3) + 5 ) * 6
        //print("in_width=\(in_width)")
       // print("in_height=\(in_height)")
        //print("in_height2=\(in_height2)")
        //collectionview
        let layout = UICollectionViewFlowLayout()
        self.collectionView1 = UICollectionView(frame: CGRectMake(0, 0, self.view.frame.size.width, in_height2), collectionViewLayout: layout)
        //注册一个cell
        self.collectionView1!.registerClass(DayCollectionViewCell.self, forCellWithReuseIdentifier:"cell")
        //注册一个headView
        self.collectionView1!.registerClass(DayReusableView.self, forSupplementaryViewOfKind:UICollectionElementKindSectionHeader, withReuseIdentifier: "DayReusableView")
        layout.headerReferenceSize = CGSizeMake(300.0,30)
        
        self.collectionView1?.delegate = self;
        self.collectionView1?.dataSource = self;
        self.collectionView1?.backgroundColor = UIColor.whiteColor()
        //设置每一个cell的宽高
        layout.itemSize = CGSizeMake((self.view.frame.size.width-60)/3, in_height)
        self.scrollview1.addSubview(self.collectionView1!)
        // Do any additional setup after loading the view.
    }

    //返回多少个组
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        
        return 1
    }
    //返回多少个cell
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return image_data.count
    }
    
    func collectionView(collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, atIndexPath indexPath: NSIndexPath) -> UICollectionReusableView{
        //  print("加载头部")
        
        var reusableview:UICollectionReusableView?
        
        if (kind == UICollectionElementKindSectionHeader)
        {
            reusableview = collectionView1!.dequeueReusableSupplementaryViewOfKind(kind, withReuseIdentifier: "DayReusableView", forIndexPath: indexPath) as! DayReusableView
            
            //reusableview?.backgroundColor = UIColor.redColor()
        }
        return reusableview!
        
    }
    
    //返回cell 上下左右的间距
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets{
        return UIEdgeInsetsMake(5, 5, 5, 5)
    }
    
    //返回自定义的cell
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("cell", forIndexPath: indexPath) as! DayCollectionViewCell
        
        cell.imgView?.image = UIImage(named: image_data[indexPath.row])
        cell.titleLabel?.text = day_data[indexPath.row]
        cell.imgView!.layer.cornerRadius = 5
        cell.imgView!.layer.masksToBounds = true
        
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
         print(indexPath.row)
       
    }
    
    func applyLayoutAttributes(layoutAttributes: UICollectionViewLayoutAttributes!){
        print("加载头部1")
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(animated: Bool) {
        
        self.scrollview1.contentSize.height = 600
    }
}
