//
//  HistoryTableViewController.swift
//  zhufu
//
//  Created by dengwei on 15/11/23.
//  Copyright © 2015年 dengwei. All rights reserved.
//

import UIKit

class HistoryTableViewController: UITableViewController {
    internal var data:[AnyObject] = [AnyObject]()
    
    @IBOutlet var table1: UITableView!
    var id:String!
    var uimageview:UIImageView!
    var noLabel:UILabel!
    private func getDefaultData(){
        
        let dalFav = FavDal()
        let count = dalFav.fetchOneForCheckIfExist("Fav").count
        if count > 0 {
            let result = dalFav.getFavGoodsList()
            if result != nil {
                self.data = result!
                self.uimageview.hidden = true
                self.noLabel.hidden = true
                self.table1.reloadData()
            }
        }else{
            
            self.table1.separatorStyle = UITableViewCellSeparatorStyle.None//隐藏表格分割线
            self.uimageview.hidden = false
            self.noLabel.hidden = false
        }
    }
    
    
    func nodata(){
        uimageview = UIImageView(frame: CGRectMake(120, 120, 100, 120))
        //设置加载一张本地图片
        //uimageview.center = self.view.center
        let image = UIImage(named:"dataEmpty.png")
        //把加载好的图片丢给imageview中的image显示
        uimageview.image = image
        //把uiimageview加载到父控件上，也就是self.view
        self.view.addSubview(uimageview)
        
        noLabel = UILabel(frame: CGRectMake(137, 245, 120, 25))
        noLabel.text = "暂无数据!"
        noLabel.textColor = UIColor.grayColor()
        // noLabel.center = self.view.center
        noLabel.font = UIFont.systemFontOfSize(14)
        self.view.addSubview(noLabel)
        uimageview.hidden = true
        noLabel.hidden = true
    }

    
    func createNoResult(){
        
        let nodata = UILabel(frame :CGRectMake(30, 30, 120, 30))
        nodata.textColor = UIColor.redColor()
        nodata.text = "暂无收藏记录!"
        self.view.addSubview(nodata)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.nodata()
        self.view.backgroundColor = UIColor.whiteColor()
        self.title = "我的收藏"
        //读取默认数据
        self.getDefaultData()
        self.table1.separatorStyle = UITableViewCellSeparatorStyle.None//隐藏表格分割线
        
        let delButton = UIBarButtonItem(title: "Del", style: UIBarButtonItemStyle.Plain, target: self, action: "DelBar:")
        
        navigationItem.rightBarButtonItem = delButton
    }
    
    
    func DelBar(sender: UIBarButtonItem){
        //设置编辑状态
        //markTag = 1
        self.table1.setEditing(!table1.editing, animated: true)
        if(table1.editing){
            sender.title = "Done"
        }else{
            sender.title = "Edit"
        }
    }
    
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(true)
        getDefaultData()
    }
    
    //分组个数
    func numberOfRowsInSection(section: Int) -> Int{
        return 1;
    }
    
    override func tableView(tableView: UITableView, editingStyleForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCellEditingStyle {
        
        return UITableViewCellEditingStyle.Delete
        
    }
    
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
    }
    
    //重写删除按钮
    override  func tableView(tableView: UITableView, titleForDeleteConfirmationButtonForRowAtIndexPath indexPath: NSIndexPath) -> String? {
        return "确认删除?"
    }
    
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        //
        let item: AnyObject = self.data[indexPath.row]
        let id = item.valueForKey("id") as? String
        let dalFav = FavDal()
        dalFav.delFavGoods(id!)
        data.removeAtIndex(indexPath.row)
        //读取默认数据
        let result = dalFav.getFavGoodsList()
        if result != nil {
            self.table1.reloadData()
        }else{
            self.table1.separatorStyle = UITableViewCellSeparatorStyle.None//隐藏表格分割线
            self.nodata()
        }
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.data.count
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 144.0
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("cell", forIndexPath: indexPath) as! HistoryTableViewCell
        
        let item: AnyObject = self.data[indexPath.row]
        let id = item.valueForKey("id") as? String
        cell.contentLabel.text = item.valueForKey("content") as? String
        cell.copyButton.tag = NSString(string: id!).integerValue
        cell.shareButton.tag = NSString(string: id!).integerValue
        cell.shareButton.accessibilityHint = item.valueForKey("content") as? String
        cell.copyButton.accessibilityHint = item.valueForKey("content") as? String
        cell.copyButton.addTarget(self, action: "copyAction:", forControlEvents: UIControlEvents.TouchUpInside)
        cell.shareButton.addTarget(self, action: "shareAction:", forControlEvents: UIControlEvents.TouchUpInside)
       
        cell.selectionStyle = .None;
        cell.updateConstraintsIfNeeded()
               
        return cell
    }
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        // print(indexPath.row)
       
    }
    func copyAction(sender:UIButton){
        // print("caoz")
        let indexPath = sender.tag
        let content = sender.accessibilityHint
        self.id = String(indexPath)
        //print("copy=\(id)")
        //  print("content=\(content!)")
        let pasteboard = UIPasteboard.generalPasteboard()
        pasteboard.string =  content!
        SVProgressHUD.showSuccessWithStatus("复制成功!")
    }

    func shareAction(sender:UIButton){
        // print("caoz")
        let indexPath = sender.tag
        let content = sender.accessibilityHint
        self.id = String(indexPath)
        print("share=\(id)")
        
        UMSocialSnsService.presentSnsIconSheetView(self, appKey: UmengConfig.UMSharedAPPKey, shareText: content!, shareImage: nil, shareToSnsNames: [UMShareToSina,UMShareToTencent,UMShareToWechatSession,UMShareToWechatTimeline,UMShareToQzone,UMShareToQQ,UMShareToRenren,UMShareToEmail,UMShareToSms,UMShareToFacebook,UMShareToTwitter], delegate: nil)
        
    }
    //下面得到分享完成的回调
    func didFinishGetUMSocialDataInViewController(response: UMSocialResponseEntity!) {
        if response.responseCode  == UMSResponseCodeSuccess{
            print("share to sns name is\(response.data)")
            SVProgressHUD.showSuccessWithStatus("分享成功!")
        }
    }

}
