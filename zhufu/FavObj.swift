//
//  FavObj.swift
//  zhufu
//
//  Created by dengwei on 15/12/1.
//  Copyright © 2015年 dengwei. All rights reserved.
//


import Foundation
import CoreData

@objc(FavObj)
public class FavObj:NSManagedObject {
    
    @NSManaged var id:String?
    @NSManaged var content:String?
    @NSManaged var addtime:String?
}