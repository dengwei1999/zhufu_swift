//
//  DayReusableView.swift
//  zhufu
//
//  Created by dengwei on 15/11/23.
//  Copyright © 2015年 dengwei. All rights reserved.
//

import UIKit

class DayReusableView: UICollectionReusableView {
    var titleLabel:UILabel?//cell上title
    var week:String!
    override init(frame: CGRect) {
        super.init(frame: frame)
        titleLabel = UILabel(frame: CGRectMake(10, 2, 280, 24))
        titleLabel?.numberOfLines = 1
        titleLabel?.font = UIFont.boldSystemFontOfSize(14.0)
        titleLabel?.textColor = UIColor.redColor()
        
        titleLabel?.text = "今天是：" + Utility.datetoString() + "," + getWeek()
       
        self.addSubview(titleLabel!)
    }
    
    func getWeek()->String{
        let dt = NSDate()
        let week = dt.dayOfWeek()
        switch week{
        case 1:
            self.week = "星期一"
            break
        case 2:
            self.week = "星期二"
            break
        case 3:
            self.week = "星期三"
            break
        case 4:
            self.week = "星期四"
            break
        case 5:
            self.week = "星期五"
            break
        case 6:
            self.week = "星期六"
            break
        case 7:
            self.week = "星期日"
            break
        default:
            self.week = "星期一"
            break
        }
        return self.week
    }
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
