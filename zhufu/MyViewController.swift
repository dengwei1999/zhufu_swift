//
//  MyViewController.swift
//  zhufu
//
//  Created by dengwei on 15/12/1.
//  Copyright © 2015年 dengwei. All rights reserved.
//

import UIKit

class MyViewController: UIViewController {

    
    @IBOutlet weak var textView1: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "关于我们"
        self.automaticallyAdjustsScrollViewInsets = false
        let content = "  送祝福短信大全是一款逢年过节方便发送祝福信息的软件。包含海量的节日祝福短信,支持直接发送短信给好友和微博等社交工具分享,并且还有查看历史记录功能,可以将你发送或查看的短信记录下来。\n  此软件操作简单,逢年过节再也不用愁发送祝福信息了!。"
        self.textView1.text = content
        self.textView1.editable = false
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
