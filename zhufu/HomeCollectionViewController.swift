//
//  HomeCollectionViewController.swift
//  zhufu
//
//  Created by dengwei on 15/11/27.
//  Copyright © 2015年 dengwei. All rights reserved.
//

import UIKit


class HomeCollectionViewController: UICollectionViewController {

    @IBOutlet var collectView1: UICollectionView!
    
    var image_data = ["logo1","logo2","logo4","logo3","logo5","logo6","logo7","logo8","logo9","logo10","logo11","logo12","logo13","logo14","logo15","logo18","logo16","logo17"]
    var day_data = ["元旦","春节","情人节","元宵节","妇女节","愚人节","劳动节","母亲节","儿童节","端午节","父亲节","七夕节","教师节","中秋节","国庆节","感恩节","光棍节","圣诞节"]
    var daytitle:String!
    var daytype:String!
    

    
    @IBAction func GoAbout(sender: UIBarButtonItem) {
        
        let toViewController1:MyViewController =  Utility.GetViewController("MyView")
        self.navigationController?.pushViewController(toViewController1, animated: true)
        //print("goto")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "主页"
         self.collectView1.backgroundColor = UIColor.whiteColor()
        
        let layout = UICollectionViewFlowLayout()
       
        self.collectView1!.registerClass(DayReusableView.self, forSupplementaryViewOfKind:UICollectionElementKindSectionHeader, withReuseIdentifier: "DayReusableView")
         layout.headerReferenceSize = CGSizeMake(300.0,20)
        
        //self.collectView1!.registerClass(FooterCollectionReusableView.self, forSupplementaryViewOfKind:UICollectionElementKindSectionFooter, withReuseIdentifier: "FooterCollectionReusableView")
        //layout.footerReferenceSize = CGSizeMake(320.0,0)
    }
/*
    
    func qmAdViewFailToLoadAd(adView: QumiBannerAD!, withError error: NSError!) {
        print("banner广告加载失败")
    }
    
    func qmAdViewClicked(adView: QumiBannerAD!) {
        print("点击banner广告")
    }
    
    func qmAdViewSuccessToLoadAd(adView: QumiBannerAD!) {
        print("banner广告加载成功")
    }*/
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize
    {
        let frame  = self.view.frame;
        var width = frame.width
        
        if (frame.width > frame.height) {
            width = frame.height
        }
        width = CGFloat(Int((width-30)/3))
        //print("width....\(width)")
        return CGSize(width: width, height: 100)
    }

    
    override func collectionView(collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, atIndexPath indexPath: NSIndexPath) -> UICollectionReusableView{
        //  print("加载头部")
        var reusableview:UICollectionReusableView?
        if (kind == UICollectionElementKindSectionHeader)
        {
            reusableview = collectView1!.dequeueReusableSupplementaryViewOfKind(kind, withReuseIdentifier: "DayReusableView", forIndexPath: indexPath) as! DayReusableView
            
            //reusableview?.backgroundColor = UIColor.redColor()
        }
        /*if (kind == UICollectionElementKindSectionFooter)
        {
            reusableview = collectView1!.dequeueReusableSupplementaryViewOfKind(kind, withReuseIdentifier: "FooterCollectionReusableView", forIndexPath: indexPath) as! FooterCollectionReusableView
            
            //reusableview?.backgroundColor = UIColor.redColor()
        }*/
        return reusableview!
    }

    //返回cell 上下左右的间距
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets{
        return UIEdgeInsetsMake(2, 2, 2, 2)
    }
    
    //返回多少个组
   override func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        
        return 1
    }
    
    //返回多少个cell
   override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return image_data.count
    }
    
//返回自定义的cell
  override  func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("cell", forIndexPath: indexPath) as! JieriCollectionViewCell
        
        cell.logoImage?.image = UIImage(named: image_data[indexPath.row])
        
        return cell
    }
    
   override func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
       // print(indexPath.row)
        let indexPath = indexPath.row
        switch indexPath{
            case 0:
                daytype = "1"
                daytitle = "元旦"
                break
            case 1:
                daytype = "2"
                daytitle = "春节"
                break
            case 2:
                daytype = "4"
                daytitle = "情人节"
            break
            case 3:
                daytype = "3"
                daytitle = "元宵节"
            break
            case 4:
                daytype = "5"
                daytitle = "妇女节"
            break
            case 5:
                daytype = "6"
                daytitle = "愚人节"
            break
            case 6:
                daytype = "7"
                daytitle = "劳动节"
            break
            case 7:
                daytype = "8"
                daytitle = "母亲节"
            break
            case 8:
                daytype = "9"
                daytitle = "儿童节"
            break
            case 9:
                daytype = "10"
                daytitle = "父亲节"
            break
            case 10:
                daytype = "11"
                daytitle = "端午节"
            break
            case 11:
                daytype = "12"
                daytitle = "七夕节"
            break
            case 12:
                daytype = "13"
                daytitle = "教师节"
            break
            case 13:
                daytype = "14"
                daytitle = "中秋节"
            break
            case 14:
                daytype = "15"
                daytitle = "国庆节"
            break
            case 15:
                daytype = "18"
                daytitle = "感恩节"
            break
            case 16:
                daytype = "16"
                daytitle = "光棍节"
            break
            case 17:
                daytype = "17"
                daytitle = "圣诞节"
            break
            default:
                daytype = "1"
                daytitle = "元旦"
            break
        }
        //print(daytitle)
    
        let viewController1 = self.storyboard?.instantiateViewControllerWithIdentifier("InfoTableView") as?InfoTableViewController
        viewController1?.daytype = daytype
        viewController1?.daytitle = daytitle
        self.navigationController?.pushViewController(viewController1!, animated: true)
    }
    
    func applyLayoutAttributes(layoutAttributes: UICollectionViewLayoutAttributes!){
        print("加载头部1")
    }


}
