//
//  InfoTableViewCell.swift
//  zhufu
//
//  Created by dengwei on 15/12/1.
//  Copyright © 2015年 dengwei. All rights reserved.
//

import UIKit

class InfoTableViewCell: UITableViewCell {

    @IBOutlet weak var bgview: UIView!
    
    @IBOutlet weak var contentLabel: UILabel!
    
    @IBOutlet weak var copyButton: UIButton!
    
    @IBOutlet weak var shareButton: UIButton!
  
    @IBOutlet weak var favButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        bgview.layer.cornerRadius = 4
        bgview.layer.borderWidth = 1
        bgview.layer.borderColor = UIColor(red: 0.85, green: 0.85, blue:0.85, alpha: 0.9).CGColor
        bgview.layer.masksToBounds = true

    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        if selected {
            self.bgview.backgroundColor = UIColor(red: 0.85, green: 0.85, blue:0.85, alpha: 0.4)
        }else{
            self.bgview.backgroundColor = UIColor(red: 0.988, green: 0.988, blue:0.988, alpha: 1)
        }
        // Configure the view for the selected state
    }

}
