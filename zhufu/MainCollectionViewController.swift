//
//  MainCollectionViewController.swift
//  zhufu
//
//  Created by dengwei on 15/11/23.
//  Copyright © 2015年 dengwei. All rights reserved.
//

import UIKit

private let reuseIdentifier = "Cell"

class MainCollectionViewController: UICollectionViewController {

    @IBOutlet var collectionView1: UICollectionView!
   
    var images = ["g1","g2","g3","g4","g5","g6","g1","g2","g3","g4","g5","g6"]
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "主页"
        self.collectionView1.backgroundColor = UIColor.whiteColor()
        
        // Register cell classes
        self.collectionView!.registerClass(UICollectionViewCell.self, forCellWithReuseIdentifier: reuseIdentifier)

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return images.count
    }
    
    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("cell", forIndexPath: indexPath) as UICollectionViewCell!
        let imageView = cell.viewWithTag(1) as? UIImageView
        imageView?.image = UIImage(named: images[indexPath.row])
        let label = cell.viewWithTag(2) as? UILabel
        label?.text = images[indexPath.row]
        return cell
    }
}
