//
//  FavDal.swift
//  zhufu
//
//  Created by dengwei on 15/12/1.
//  Copyright © 2015年 dengwei. All rights reserved.
//


import Foundation
import CoreData


class FavDal:NSObject {
    
    func addFavInfo(id:String,content:String,addtime:String){
        //添加关注
        let context=CoreDataManager.shared.managedObjectContext;
        let model = NSEntityDescription.entityForName("Fav", inManagedObjectContext: context)
        let fav = FavObj(entity: model!, insertIntoManagedObjectContext: context)
        if model != nil {
            
            self.objManagedObject(id,content:content,addtime:addtime,fav:fav)
            CoreDataManager.shared.save()
        }
    }
    
    func getFavGoodsList()->[AnyObject]? {
        //关注列表
        let request = NSFetchRequest(entityName: "Fav")
        let sort1=NSSortDescriptor(key: "id", ascending: false)
        request.fetchLimit = 30
        request.sortDescriptors = [sort1]
        request.resultType = NSFetchRequestResultType.DictionaryResultType
        let result = CoreDataManager.shared.executeFetchRequest(request)
        return result
    }
    
    func deleteAll(){
        //删除表
        CoreDataManager.shared.deleteTable("Fav")
    }
    
    //删除单条数据
    func delFavGoods(id:String){
        let context=CoreDataManager.shared.managedObjectContext;
        let entityDescription = NSEntityDescription.entityForName("Fav", inManagedObjectContext: context)
        let request = NSFetchRequest()
        request.entity = entityDescription
        request.predicate = NSPredicate(format: "id == %@", id)
        do {
            let objects:Array = try context.executeFetchRequest(request)
            
            for resultItem in objects {
                context.deleteObject(resultItem as! NSManagedObject)
            }
            CoreDataManager.shared.save()
            // print("删除成功")
        } catch _ {
            //print("修改失败")
        }
        
    }
    
    //按条件检查entity是否有符合条件数据
    func fetchOneWithConditionForCheckIfExist(entityName:String,condition:NSPredicate)->[AnyObject]! {
        let fetchReq = NSFetchRequest(entityName: entityName)
        fetchReq.predicate = condition
        fetchReq.fetchLimit = 1
        //var error:NSError? = nil
        return CoreDataManager.shared.executeFetchRequest(fetchReq)
    }
    //检查entity里是否有数据
    func fetchOneForCheckIfExist(entityName:String)->[AnyObject]! {
        let fetchReq = NSFetchRequest(entityName: entityName)
        fetchReq.fetchLimit = 1
        // var error:NSError? = nil
        return CoreDataManager.shared.executeFetchRequest(fetchReq)
    }
    
    func insertForEntityWithName(entityName:String)->AnyObject! {
        return NSEntityDescription.insertNewObjectForEntityForName(entityName, inManagedObjectContext: CoreDataManager.shared.managedObjectContext)
    }
    //判断是否存在某条记录
    func isExistFav(id:String)->[AnyObject]?{
        let context=CoreDataManager.shared.managedObjectContext;
        let entityDescription = NSEntityDescription.entityForName("Fav", inManagedObjectContext: context)
        let request = NSFetchRequest()
        request.entity = entityDescription
        request.predicate = NSPredicate(format: "id == %@", id)
        request.fetchLimit = 1
        return CoreDataManager.shared.executeFetchRequest(request)
    }
    //查询单条数据
    func getFavList(id:String)->[AnyObject]?{
        let context=CoreDataManager.shared.managedObjectContext;
        let entityDescription = NSEntityDescription.entityForName("Fav", inManagedObjectContext: context)
        let request = NSFetchRequest()
        request.entity = entityDescription
        request.predicate = NSPredicate(format: "id == %@", id)
        let result =     CoreDataManager.shared.executeFetchRequest(request)
        for data in result! {
            let ids = data.valueForKey("id")
            let content = data.valueForKey("content")
            let addtime = data.valueForKey("addtime")
            print("ids: \(ids!), \(content!), \(addtime!)")
        }
        return result
    }
    
    func updateFavGoods(id:String,content:String,addtime: String){
        
        let context=CoreDataManager.shared.managedObjectContext;
        let entityDescription = NSEntityDescription.entityForName("Fav", inManagedObjectContext: context)
        let request = NSFetchRequest()
        request.entity = entityDescription
        request.predicate = NSPredicate(format: "id == %@", id)
        
        do {
            let objects:Array = try context.executeFetchRequest(request)
            
            for obj in objects {
                obj.setValue(content, forKey: "content")
                obj.setValue(addtime,forKey:"addtime")
            }
            CoreDataManager.shared.save()
            //print("修改成功")
        } catch _ {
            // print("修改失败")
        }
    }
    
    func objManagedObject(id:String,content:String,addtime:String,fav:FavObj) -> FavObj{
        
        fav.id = id
        fav.content = content
        fav.addtime = addtime
        return fav;
    }
    
}