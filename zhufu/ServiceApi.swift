//
//  Services.swift
//  zhufu
//
//  Created by dengwei on 15/11/28.
//  Copyright © 2015年 dengwei. All rights reserved.
//

import UIKit

class ServiceApi: NSObject {
    static var host:String = "http://www.dengwei1999.com"
    
    //主页url
    internal class func getInfoList(tid:Int,maxId:Int)->String{
        return "\(host)/zhufu_ios.php?format=getNewsList&tid=\(tid)&lastid=\(maxId)"
    }

    
    //推荐短信
    internal class func getTuijianList(maxId:Int)->String{
        return "\(host)/zhufu_ios.php?format=getNewsList&lastid=\(maxId)"
    }

}
