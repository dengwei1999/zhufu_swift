//
//  TuijianTableViewController.swift
//  zhufu
//
//  Created by dengwei on 15/11/23.
//  Copyright © 2015年 dengwei. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class TuijianTableViewController: UITableViewController,QumiBannerADDelegate {

    @IBOutlet var table1: UITableView!
    var qumiBannerAD:QumiBannerAD?
    var id:String!
    internal var data:[AnyObject] = [AnyObject]()
    var loading:Bool = false
    var type:Int!
    var settitle:String!
    var uimageview:UIImageView!
    var noLabel:UILabel!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "推荐短信"
        self.table1.estimatedRowHeight = 94;
        self.table1.rowHeight = UITableViewAutomaticDimension
        nodata()
        //创建一个banner条广告视图
        self.qumiBannerAD = QumiBannerAD()
        self.qumiBannerAD?.cxInitWithQumiBannerAD()
        //放置广告条的位置
        self.qumiBannerAD!.frame = CGRectMake(0, 20, 320, 50)
        //设置代理
        self.qumiBannerAD!.delegate = self
        self.qumiBannerAD!.loadBannerAd(true)
        self.qumiBannerAD!.rootViewController = self
        //开始加载和展示广告 如果请求广告，请填写YES，如果不请求广告，请填写NO
       // self.view.addSubview(self.qumiBannerAD!)
        self.table1.tableHeaderView = self.qumiBannerAD!
        
        self.table1.addHeaderWithCallback{
            
            self.loadData(0, isPullRefresh: true)
        }
        
        self.table1.addFooterWithCallback{
            
            if(self.data.count>0) {
                let id = self.data.last!.valueForKey("id") as! String
                let  maxId = NSString(string: id).integerValue
                // let  maxid2 = self.data.last!.valueForKey("id") as! Int
                // print(maxId)
                self.loadData(maxId, isPullRefresh: false)
            }
        }
        
        self.table1.headerBeginRefreshing()
        table1.separatorStyle = UITableViewCellSeparatorStyle.None//隐藏表格分割线
    }

    
    func qmAdViewFailToLoadAd(adView: QumiBannerAD!, withError error: NSError!) {
        print("banner广告加载失败")
    }
    
    func qmAdViewClicked(adView: QumiBannerAD!) {
        print("点击banner广告")
    }
    
    func qmAdViewSuccessToLoadAd(adView: QumiBannerAD!) {
        print("banner广告加载成功")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func nodata(){
        uimageview = UIImageView(frame: CGRectMake(120, 120, 100, 120))
        //设置加载一张本地图片
        //uimageview.center = self.view.center
        let image = UIImage(named:"dataEmpty.png")
        //把加载好的图片丢给imageview中的image显示
        uimageview.image = image
        //把uiimageview加载到父控件上，也就是self.view
        self.view.addSubview(uimageview)
        
        noLabel = UILabel(frame: CGRectMake(137, 245, 120, 25))
        noLabel.text = "暂无数据!"
        noLabel.textColor = UIColor.grayColor()
        // noLabel.center = self.view.center
        noLabel.font = UIFont.systemFontOfSize(14)
        self.view.addSubview(noLabel)
        uimageview.hidden = true
        noLabel.hidden = true
    }
    
    //分组个数
    func numberOfRowsInSection(section: Int) -> Int{
        return 1;
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.data.count
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 144.0
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("cell", forIndexPath: indexPath) as! DuanxinTableViewCell
        
        let item: AnyObject = self.data[indexPath.row]
        let id = item.valueForKey("id") as? String
        cell.contentLabel.text = item.valueForKey("content") as? String
        cell.copyButton.tag = NSString(string: id!).integerValue
        cell.shareButton.tag = NSString(string: id!).integerValue
        cell.favButton.tag = NSString(string: id!).integerValue
        cell.favButton.accessibilityHint = item.valueForKey("content") as? String
        cell.copyButton.accessibilityHint = item.valueForKey("content") as? String
        cell.shareButton.accessibilityHint = item.valueForKey("content") as? String
        cell.copyButton.addTarget(self, action: "copyAction:", forControlEvents: UIControlEvents.TouchUpInside)
        cell.shareButton.addTarget(self, action: "shareAction:", forControlEvents: UIControlEvents.TouchUpInside)
        cell.favButton.addTarget(self, action: "favAction:", forControlEvents: UIControlEvents.TouchUpInside)
        cell.selectionStyle = .None;
        cell.updateConstraintsIfNeeded()
        let favDal = FavDal()
        let fav_count = favDal.isExistFav(id!)?.count
        if fav_count! > 0{
            cell.favButton.setImage(UIImage(named:"shoucang_2"),forState:.Selected)
            cell.favButton.selected = true
        }else{
            cell.favButton.setImage(UIImage(named:"shoucang_2"),forState:.Selected)
            cell.favButton.selected = false
        }
        
        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        print(indexPath.row)
        
    }
    func copyAction(sender:UIButton){
        // print("caoz")
        let indexPath = sender.tag
        let content = sender.accessibilityHint
        self.id = String(indexPath)
        //print("copy=\(id)")
      //  print("content=\(content!)")
        let pasteboard = UIPasteboard.generalPasteboard()
        pasteboard.string =  content!
        SVProgressHUD.showSuccessWithStatus("复制成功!")
    }
    
    func favAction(sender:UIButton){
        // print("caoz")
        let indexPath = sender.tag
        let content = sender.accessibilityHint
        self.id = String(indexPath)
        print("fav=\(id)")
        
        //关注按钮
        let  now = NSDate()
        let dateFormatter2 = NSDateFormatter()
        dateFormatter2.dateFormat = "yyyy-MM-dd HH:mm:ss"
        // Date 转 String
        let nowString = dateFormatter2.stringFromDate(now)
        let favDal = FavDal()
        
        let fav_count = favDal.isExistFav(self.id)?.count
        if fav_count! > 0{
            favDal.delFavGoods(self.id)
            sender.setImage(UIImage(named:"shoucang_2"),forState:.Selected)
            sender.selected = false
            SVProgressHUD.showSuccessWithStatus("取消收藏!")
        }else{
            favDal.addFavInfo(self.id,content: content!,addtime: nowString)
            sender.setImage(UIImage(named:"shoucang_2"),forState:.Selected)
            sender.selected = true
            SVProgressHUD.showSuccessWithStatus("收藏成功!")
        }
    }
    func shareAction(sender:UIButton){
        // print("caoz")
        let indexPath = sender.tag
        let content = sender.accessibilityHint
        self.id = String(indexPath)
       // print("share=\(id)")
        
        UMSocialSnsService.presentSnsIconSheetView(self, appKey: UmengConfig.UMSharedAPPKey, shareText: content!, shareImage: nil, shareToSnsNames: [UMShareToSina,UMShareToTencent,UMShareToWechatSession,UMShareToWechatTimeline,UMShareToQzone,UMShareToQQ,UMShareToRenren,UMShareToEmail,UMShareToSms,UMShareToFacebook,UMShareToTwitter], delegate: nil)
        
    }
    //下面得到分享完成的回调
    func didFinishGetUMSocialDataInViewController(response: UMSocialResponseEntity!) {
        if response.responseCode  == UMSResponseCodeSuccess{
            print("share to sns name is\(response.data)")
            SVProgressHUD.showSuccessWithStatus("分享成功!")
        }
    }
    func loadData(maxId:Int, isPullRefresh:Bool){
        if self.loading {
            return
        }
        self.loading = true
        Alamofire.request(.GET,ServiceApi.getTuijianList(maxId)).responseJSON{
            closureResponse in
                        
            self.loading = false
            
            if(isPullRefresh){
                self.table1.headerEndRefreshing()
            }
            else{
                self.table1.footerEndRefreshing()
            }
            if closureResponse.result.isFailure {
               
                SVProgressHUD.showInfoWithStatus("请检查网络设置!")
                return
            }
            
            var result = JSON(closureResponse.result.value!)
            if result["ret"].intValue == 1 {
                
                let items = result["data"].object as! [AnyObject]
                print(items.count)
                if(items.count==0){
                    return
                }
                
                if(isPullRefresh){
                    
                    self.data.removeAll(keepCapacity: false)
                }
                
                for  it in items {
                    
                    self.data.append(it);
                }
                dispatch_async(dispatch_get_main_queue()) {
                    self.uimageview.hidden = true
                    self.noLabel.hidden = true
                    self.table1.reloadData()
                }
                
            }else  if result["ret"].intValue == 2 {
                
                SVProgressHUD.showInfoWithStatus("已是最后一页!")
                return
            }else{
                if(isPullRefresh){
                    self.data.removeAll(keepCapacity: false)
                }
                self.table1.reloadData()
                self.table1.separatorStyle = UITableViewCellSeparatorStyle.None//隐藏表格分割线
                self.uimageview.hidden = false
                self.noLabel.hidden = false
                return
            }
        }
    }

}
