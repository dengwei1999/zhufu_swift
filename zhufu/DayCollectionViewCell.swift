//
//  DayCollectionViewCell.swift
//  zhufu
//
//  Created by dengwei on 15/11/23.
//  Copyright © 2015年 dengwei. All rights reserved.
//

import UIKit

class DayCollectionViewCell: UICollectionViewCell {

    let width = UIScreen.mainScreen().bounds.size.width//获取屏幕宽
    var imgView : UIImageView?//cell上的图片
    var titleLabel:UILabel?//cell上title
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        //初始化各种控件
        imgView = UIImageView(frame: CGRectMake(5, 5, (width-60)/3, ((width-60)/3)+10))
        self.addSubview(imgView!)
        //titleLabel = UILabel(frame: CGRectMake(8, CGRectGetMaxY(imgView!.frame)-2, (width-40)/2, 30))
        //titleLabel?.numberOfLines = 0
        //titleLabel?.font = UIFont.boldSystemFontOfSize(14.0)
        //titleLabel?.font = UIFont.systemFontOfSize(14.0)
        //titleLabel?.textColor = UIColor.lightGrayColor()
        //self.addSubview(titleLabel!)
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }


}
